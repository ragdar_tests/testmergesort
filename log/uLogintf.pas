﻿unit uLogintf;

interface
uses
  Windows, uDateUtils;
type
{ lmNone 0, lmDebug 10, lmTrace 20, lmInfo 30, lmWarning 40, lmError 50, lmCritical 60, lmFatal 70}
  TLogMode = (lmNone, lmDebug, lmTrace, lmInfo, lmWarning, lmError, lmCritical, lmFatal);

//  //Глобальный, выдает синглтоны по имени объекта Parent.Child
//  ILogging = interface
//    function GetLogger(ObjPath: string): ILogger;
//    function GetLogFile(AName: string): ILogger; // Логгер с пустым именем объекта
//  end;
//
//  ILogger = interface
//    procedure Log(const AMode: TLogMode; const AMsg: string); overload;
//    procedure Log(const AMode: TLogMode; const Format: string; const Args: array of const); overload;
//    procedure Log(const AMode: TLogMode; const Args: array of variant); overload;
//    function GetChild(AObjName: string): ILogger; // лог следующего уровня
//    function GetLog(AObjName: string): ILogger; // лог другого обекта
//  end;
  ILog = interface
    procedure Log(const AMode: TLogMode; const AMsg: string); overload;
    procedure Log(const AMode: TLogMode; const AFormat: string; const Args: array of const); overload;
    procedure Log(const AMode: TLogMode; const Args: array of variant); overload;
    //function GetParent(): ILog; // Заменить имя последнего объекта
    function GetLog(AObjName: string): ILog; // лог следующего уровня
    function GetRoot(AObjName: string): ILog; // лог другого обекта
    function GetSib(AObjName: string): ILog; // Заменить имя последнего объекта
  end;

  IWriter = interface
    function Save(Msg: string): boolean;
    function IsEmpty(): boolean;// Для записи заголовка
  end;

  IWriterFactory = interface
    function GetWriter(AFileName: string): IWriter; //писатель в новый файл
  end;

  ILogRoot = interface
    procedure Log(const AMode: TLogMode; const ObjectName, AMsg: string);
//    function GetLogFile(AFileName: string): ILogRoot; //Лог в другой файл
  end;

  ILogData = interface
    procedure NewSysDate();
    function GetSysDateTime: PSysDateTime;
    function GetSysDate(): TSysDate;
    function GetSysTime(): TSysTime;
    function GetHead(): string;
//    function GetObjectName
  end;

  ILogWriter = interface
    procedure WriteLn(Msg: string);
//    procedure SetData(ALogData: ILogData);
  end;

  IFileWriter = interface
    function Save(Msg: string): boolean;
    function WriteLn(Msg: string): boolean;
    function IsEmpty(): boolean;// Для записи заголовка
    function FileSize: Longint;
  end;

  IFileWriterFactory = interface
    function GetFileWriter(AFileName: string): IFileWriter; //писатель в новый файл
  end;

  ILogging = interface
    function GetLog(AUnitName: string): ILog;
  end;


implementation

end.
