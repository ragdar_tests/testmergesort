unit uSortList;

interface
type
  ISortList = interface
    function Compare(Index1, Index2: Integer): Integer;
    procedure ExchangeItems(Index1, Index2: Integer);
  end;

  IUniSort = interface
    procedure Sort(List: ISortList; L, R: Integer);
  end;

  TUniSort = class(TInterfacedObject, IUniSort)
  private
    FList: ISortList;
  public
    procedure Sort(List: ISortList; L, R: Integer);
    procedure QuickSort(L, R: Integer);
  end;

implementation

procedure TUniSort.Sort(List: ISortList; L, R: Integer);
begin
  FList := List;
  QuickSort(L,R);
end;

{
procedure TStringList.QuickSort(L, R: Integer; SCompare: TStringListSortCompare);
var
  I, J, P: Integer;
begin
  repeat
    I := L;
    J := R;
    P := (L + R) shr 1;
    repeat
      while SCompare(Self, I, P) < 0 do Inc(I);
      while SCompare(Self, J, P) > 0 do Dec(J);
      if I <= J then
      begin
        if I <> J then
          ExchangeItems(I, J);
        if P = I then
          P := J
        else if P = J then
          P := I;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then QuickSort(L, J, SCompare);
    L := I;
  until I >= R;
end;
}
procedure TUniSort.QuickSort(L, R: Integer);
var
  I, J, P: Integer;
begin
  repeat
    I := L;
    J := R;
    P := (L + R) shr 1;
    repeat
      while FList.Compare(I, P) < 0 do
        Inc(I);
      while FList.Compare(J, P) > 0 do
        Dec(J);
      if I <= J then
      begin
        if I <> J then
          FList.ExchangeItems(I, J);
        if P = I then
          P := J
        else if P = J then
          P := I;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then
      QuickSort(L, J);
    L := I;
  until I >= R;
end;

end.
