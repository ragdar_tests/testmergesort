﻿unit uLogWriterFile;

interface
uses
  System.Types, uFileUtils,
  windows, sysutils, uLogintf, uDateUtils, classes;

type
  INotifyFree = interface
    procedure ItemFree(AObj: TObject);
  end;

  TFileWriterFactory = class(TInterfacedObject, IFileWriterFactory, INotifyFree)
  private
    FLogPath: string;
    FWriters: TList;
  protected
    { INotifyFree }
    procedure ItemFree(AObj: TObject);
    { /INotifyFree }
  public
    constructor Create(ALogPath: string);
    destructor Destroy; override;
    { IWriterFactory }
    function GetFileWriter(AFileName: string): IFileWriter; //писатель в новый файл
    { /IWriterFactory }
  end;

  TFileWriter = class(TInterfacedObject, IFileWriter)
  private
    FFileHandle: THandle;
    FExtraFileHandle: THandle;
    FFileName: string;
    FRecipient: INotifyFree;
  protected
    function Open: boolean;
    { IFileWriter }
    function Save(Msg: string): boolean;
    function WriteLn(Msg: string): boolean;
    function IsEmpty(): boolean;
    function FileSize: Longint;
    { /IFileWriter }
    procedure SetRecipient(ARecipient: INotifyFree);
  public
    constructor Create(AFileName: string);
    destructor Destroy; override;
  end;

  TLogWriterDay = class(TInterfacedObject, ILogWriter)
  private
    FFileIndex: Integer;
    FLastDate: TSysDate;
    FLogData: ILogData;
    FUnitName: string;
    FWriter: IFileWriter;
    FFileFactory: IFileWriterFactory;
    procedure CheckTime();
    procedure CheckSize();
    //procedure NewDay(ADate: TSysDate);
    procedure OpenWriter();
    function GetFileName(): string;
  protected
    { IWriter }
    procedure WriteLn(Msg: string);
//    procedure SetData(ALogData: ILogData);
    { /IWriter }
  public
    constructor Create(AUnitName: string; AFileFactory: IFileWriterFactory; ALogData: ILogData);
    destructor Destroy; override;
  end;

implementation

function OpenFile(const AFileName: string): THandle;
begin
  Result := CreateFile(PChar(AFileName), GENERIC_WRITE, FILE_SHARE_READ or FILE_SHARE_WRITE {or FILE_SHARE_DELETE},
   nil, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL {or FILE_FLAG_WRITE_THROUGH}, 0);
end;

{ TWriterFactory }

constructor TFileWriterFactory.Create(ALogPath: string);
begin
  FLogPath := ALogPath;
  FWriters := TList.Create;
end;

destructor TFileWriterFactory.Destroy;
begin
  FWriters.Free;
  inherited;
end;

function TFileWriterFactory.GetFileWriter(AFileName: string): IFileWriter;
var
  i: Integer;
  Writer: TFileWriter;
begin
  AFileName := GetFullFileName(AFileName, FLogPath, FLogPath);
  Result := nil;
  for i := 0 to FWriters.Count - 1 do
  begin
    Writer := TFileWriter(FWriters[i]);
    if Writer.FFileName = AFileName then
    begin
      Result := Writer;
      break;
    end;
  end;
  if Result = nil then
  begin
    Writer := TFileWriter.Create(AFileName);
    Result := Writer;
    Writer.SetRecipient(self);
    FWriters.Add(Pointer(Writer))
  end;
end;

procedure TFileWriterFactory.ItemFree(AObj: TObject);
begin
  FWriters.Remove(AObj);
end;

{ TWriter }

constructor TFileWriter.Create(AFileName: string);
begin
  FFileName := AFileName;
  Open();
end;

destructor TFileWriter.Destroy;
begin
  if FRecipient <> nil then
    FRecipient.ItemFree(self);
  inherited;
end;

procedure TFileWriter.SetRecipient(ARecipient: INotifyFree);
begin // + AddRef
  FRecipient := ARecipient;
end;

function TFileWriter.WriteLn(Msg: string): boolean;
begin
  result := Save(Msg + #13#10);
end;

function TFileWriter.IsEmpty: boolean;
begin
  if FFileHandle <> 0 then
    result := SetFilePointer(FFileHandle, 0, nil, FILE_END) = 0
  else
    result := false
end;

function TFileWriter.FileSize: Longint;
begin
  if FFileHandle <> 0 then
    result := SetFilePointer(FFileHandle, 0, nil, FILE_END)
  else
    result := 0;
end;

function GetErrorMessage(LastError: Integer): string;
begin
  if LastError <> 0 then
    Result := 'System Error.  Code: ' + inttostr(LastError) + '. ' + SysErrorMessage(LastError)
  else
    Result := 'Unknown OS Error';
end;

function TFileWriter.Open: boolean;
// Передать сообщение об ошибке!
var
  i: integer;
  ErrorMessage: string;
begin
  Result := true;
  if (FFileHandle = 0) and (FFileName <> '') then //Если Файл еще не открыт и имя задано то создать файл лога
  begin
    FFileHandle := OpenFile(FFileName);
    Result := FFileHandle <> INVALID_HANDLE_VALUE;
    if Result then
      SetFilePointer(FFileHandle, 0, nil, FILE_END)
    else
    begin
      FFileHandle := 0;
      ErrorMessage := GetErrorMessage(GetLastError);
      for i := 2 to 3 do
      begin
        FExtraFileHandle := OpenFile(FFileName + '.' + inttostr(i));
        if FExtraFileHandle <> INVALID_HANDLE_VALUE then
        begin
          SetFilePointer(FFileHandle, 0, nil, FILE_END);
          Break
        end
        else
          FExtraFileHandle := 0
      end;
    end;
  end;
end;

function TFileWriter.Save(Msg: string): boolean;
var
  WriteBytes: Cardinal;
begin
  Result := false;
  if FFileHandle = 0 then
    Open();
  if FFileHandle <> 0 then
  begin
//    SetFilePointer(FFileHandle, 0, nil, FILE_END);
    Result := WriteFile(FFileHandle, pchar(Msg)^, ByteLength(Msg), WriteBytes, nil);
//    if not Result then
//    begin
//      FLastErrorMessage := GetErrorMessage(GetLastError);
//    end;
  end;
end;

{ TLogWriter }

constructor TLogWriterDay.Create(AUnitName: string; AFileFactory: IFileWriterFactory; ALogData: ILogData);
begin
  FUnitName := AUnitName;
  FFileFactory := AFileFactory;
  FLogData := ALogData;
end;

destructor TLogWriterDay.Destroy;
begin
//  pointer(FLogData) := nil;
  inherited;
end;

//procedure TLogWriterDay.SetData(ALogData: ILogData);
//begin
//  pointer(FLogData) := pointer(ALogData);
//end;

procedure TLogWriterDay.WriteLn(Msg: string);
begin
  CheckTime;
  CheckSize;
  FWriter.WriteLn(Msg)
end;

procedure TLogWriterDay.OpenWriter();
begin
  FWriter := FFileFactory.GetFileWriter(GetFileName);
  if FWriter.IsEmpty then
    FWriter.WriteLn(FLogData.GetHead);
end;

procedure TLogWriterDay.CheckSize();
begin
  while (FWriter.FileSize > 10*1024*1024) and (FFileIndex < High(FFileIndex)) do
  begin
    Inc(FFileIndex);
    OpenWriter();
  end;
//    NewDay(FLogData.GetSysDate);
end;

procedure TLogWriterDay.CheckTime();
begin
  if int64(FLastDate) <> int64(FLogData.GetSysDate) then
  begin
    FFileIndex := 0;
    FLastDate := FLogData.GetSysDate;
    OpenWriter();
  end;
end;

//procedure TLogWriterDay.NewDay(ADate: TSysDate);
//begin
//  FWriter := FFileFactory.GetFileWriter(GetFileName(SysDateToIso(ADate)));
//  if FWriter.IsEmpty then
//    FWriter.WriteLn(FLogData.GetHead);
//  FLastDate := ADate;
//end;

function TLogWriterDay.GetFileName(): string;
var
  preffix, DateStr, FileIndexStr: string;
begin
  if FUnitName = '' then
    preffix := ''
  else
    preffix := FUnitName + '-';

  if FFileIndex > 0 then
    FileIndexStr :=  '.' + inttostr(FFileIndex)
  else
    FileIndexStr := '';

  DateStr := SysDateToIso(FLogData.GetSysDate);

  Result := preffix + DateStr + FileIndexStr + '.log'; //  'log\Modem-2014-12-43.2.log';
end;

end.
