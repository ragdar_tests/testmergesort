unit uTestUtils;

interface
uses
  uTest, System.SysUtils;

procedure SetLogTest(ALogTest: ILogTest);

procedure RowError(FileName, msg: string; RowID: integer);

var
  LogTest: ILogTest;

implementation

procedure SetLogTest(ALogTest: ILogTest);
begin
  LogTest := ALogTest;
end;

procedure RowError(FileName, msg: string; RowID: integer);
begin
  LogTest.AddMsg('Error in ' + fileName + '. ' + msg + '. Row='+ IntTostr(RowID + 1));
end;

end.
