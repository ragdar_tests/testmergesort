unit uLogging;

interface

uses
  uLogIntf, uLogWriterFile, uLogRoot, uLogObj;

type
  TLogging = class(TInterfacedObject, ILogging)
  private
    FFileWriterFactory: IFileWriterFactory;
  protected
    function GetLog(UnitName: string): ILog;
  public
    constructor Create(ALogPath: string);
    destructor Destroy; override;
  end;

implementation

constructor TLogging.Create(ALogPath: string);
begin
  FFileWriterFactory := TFileWriterFactory.Create(ALogPath);
end;


destructor TLogging.Destroy;
begin
  inherited;
end;

function TLogging.GetLog(UnitName: string): ILog;
var
  LogRoot: ILogRoot;
  LogWriter: ILogWriter;
  LogData: ILogData;
begin
  LogData := TLogData.Create();
  LogWriter := TLogWriterDay.Create(UnitName, FFileWriterFactory, LogData);
  LogRoot := TLogRoot.Create(LogWriter, LogData);
  Result := TLogObj.Create(LogRoot, '');
end;

end.
