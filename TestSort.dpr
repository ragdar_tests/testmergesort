program TestSort;

uses
  Vcl.Forms,
  uMainForm2,
  uTest,
  uTestMan,
  uLogging in 'log\uLogging.pas',
  uLogintf in 'log\uLogintf.pas',
  uLogObj in 'log\uLogObj.pas',
  uLogRoot in 'log\uLogRoot.pas',
  uLogWriterFile in 'log\uLogWriterFile.pas',
  uAppUtils in 'uAppUtils.pas',
  uTextUtils in 'uTextUtils.pas',
  uTestSort in 'uTestSort.pas',
  uSortList in 'uSortList.pas',
  uSortInt in 'uSortInt.pas',
  uMergeSort in 'uMergeSort.pas',
  uSortStr in 'uSortStr.pas',
  uMergeSortStr in 'uMergeSortStr.pas';

{$R *.res}

procedure main();
var
  fmMain: TfmMain;
  Logging: ILogging;
  MainLog: ILog;
begin
  Application.Initialize;
  Application.CreateForm(TfmMain, fmMain);
  Logging := TLogging.Create(GetLogPath);
  MainLog := Logging.GetLog('testing');
  fmMain.AddTestObject(TTestSort.Create(MainLog));
  fmMain.StartAllTest();
  Application.Run;
end;

begin
  main;
end.
