unit uMergeSortStr;

interface

uses
  System.SysUtils, System.Types, System.Classes,
  uMergeSort;

type
  TMergeSortStr = class(TInterfacedObject, IMergeSortList)
  private
    FList: TStrings;
    FList2: TStrings;
    FListExternal: TStrings;
    FListInternal: TStrings;
    procedure Swap();
    function Compare(Index1, Index2: Integer): Integer;
    procedure SaveItem(Dest, Src: Integer);
    procedure SaveItems(Dest, Src, Count: Integer);
  public
    constructor Create(const AList: TStrings);
    destructor Destroy; override;
  end;

implementation

function CompareIntegerAsc(Item1, Item2: Integer): Integer;
begin
  if Item1>Item2 then
    Result := 1
  else
  if Item1<Item2 then
    Result := -1
  else
    Result := 0;
end;

{ TSortGroups }
function TMergeSortStr.Compare(Index1, Index2: Integer): Integer;
begin
  Result := CompareStr(FList[Index1], FList[index2]);
end;

constructor TMergeSortStr.Create(const AList: TStrings);
var
  i: integer;
begin
  FListExternal := AList;
  FList := AList;
  FListInternal := TStringList.Create;
  for i := 0 to FListExternal.Count - 1 do
    FListInternal.Add('');
  FList2 := FListInternal;
//  FList2.Capacity := FList.Count;
end;

destructor TMergeSortStr.Destroy;
begin
  FList2.Free;
  inherited;
end;

procedure TMergeSortStr.SaveItem(Dest, Src: Integer);
begin
  FList2[Dest] := FList[Src];
end;

procedure TMergeSortStr.SaveItems(Dest, Src, Count: Integer);
var
  i: integer;
begin
  for I := 0 to Count - 1 do
    FList2[Dest + i] := FList[Src + I];
end;

procedure TMergeSortStr.Swap;
var
  List: TStrings;
begin
  List := FList;
  FList := FList2;
  FList2 := List;
end;

end.
