﻿unit uTestMan;

interface

uses
  Windows, Classes, SysUtils, uTest, uDateUtils;

type
  TTestManager = class(TInterfacedObject, ILogTest)
  private
    FBufLines: TStrings;
    FOutLines: TStrings;
    FTestCount, FErrorCount: integer;
    //FFirstTickCount, FLastTickCount: Integer;
    FFirstTSC, FLastTSC: Int64;
    FShowOK: Boolean;
  public
    procedure Test(Descr: string; Succesful: Boolean);
    procedure StartTest(Descr: string);
    procedure EndTest(Descr: string);
    procedure AddMsg(Descr: string);
    constructor Create(AOutLines: TStrings);
    destructor Destroy();override;
    procedure SetOutStrings(AOutStrings: TStrings);
    procedure AddLog(s: string);
    procedure Flush();
    procedure SetShowOK(aShowOK: Boolean);
  end;

implementation

constructor TTestManager.Create(AOutLines: TStrings);
begin
  SetOutStrings(AOutLines);
  FBufLines := TStringList.Create();
end;

procedure TTestManager.AddMsg(Descr: string);
begin
  FBufLines.Add(Descr);
end;

procedure TTestManager.Test(Descr: string; Succesful: Boolean);
var
  msg: string;
begin
  Inc(FTestCount);
  if Succesful then
    msg := 'ОК'
  else
  begin
    Inc(FErrorCount);
    msg := 'Error';
  end;
  if not Succesful or FShowOK then
    AddLog(Descr + ' - ' + msg);
end;

procedure TTestManager.EndTest(Descr: string);
var
  LastTSC: Int64;
begin
  LastTSC := Rdtsc();
  AddLog('= Конец теста ' + Descr + ', Ошибок:' + IntToStr(FErrorCount) + ', ВремяTSC:' + IntToStr(LastTSC - FFirstTSC));
// Test('= Конец теста ' + Descr + ', Ошибок:' + IntToStr(FErrorCount) + ', Время:' + IntToStr(FLastTSC-FFirstTSC),  FErrorCount = 0);
  FErrorCount := 0;
  Flush;
end;

procedure TTestManager.AddLog(s: string);
var
  TSC: int64;
  dx: int64;
begin
  TSC := Rdtsc();
  dx := 0;
  if FLastTSC > 0 then
    dx := TSC - FLastTSC;
  FLastTSC := TSC;
//  s := GetNowStr() + ' [] ' + s;
 // ThousandSeparator := ' ';
  S := Format('%s [%12.0N] %s', [GetNowStrISO(), dx+0., s]);
  FBufLines.Add(s);
//  S := s + #13#10;
//  fs.Write(s[1],Length(s));
//  FlushFileBuffers(fs.Handle);
end;

procedure TTestManager.StartTest(Descr: string);
begin
  //GetNanoTime()
  FFirstTSC := rdtsc();
  AddLog('= Начало теста '+Descr);
end;

procedure TTestManager.SetOutStrings(AOutStrings: TStrings);
begin
  FOutLines := AOutStrings;
end;

destructor TTestManager.Destroy;
begin
  FreeAndNil(FBufLines);
  inherited;
end;

procedure TTestManager.Flush;
begin
  FOutLines.AddStrings(FBufLines);
  FBufLines.Clear;
end;

procedure TTestManager.SetShowOK(aShowOK: Boolean);
begin
  FShowOK := aShowOK;
end;

end.

