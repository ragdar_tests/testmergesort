unit uTestSort;

interface
uses
  System.Classes, System.SysUtils, System.Types,
  uSortInt, uSortStr, uSortList, uMergeSort, uMergeSortStr,
  uTest, uLogintf, uDateUtils, uTextUtils, uAppUtils;

type
  TTestSort = class(TInterfacedObject, ITest)
  private
    FLog: ILog;
    FList: TIntegerDynArray;
    FUList: TStringList;
    FMList: TStringList;
  protected
    FLogTest: ILogTest;
    procedure testSort();
    procedure TestUniSortInt;
    procedure TestUniSortStr;
    procedure TestMSortStr();
    function GetRandomStr(): AnsiString;
    procedure PrintMList;
    procedure PrintUList;
  public
    constructor Create(ALog: ILog);
    destructor Destroy(); override;
    { ITest }
    function GetCaption: widestring;
    procedure TestProc(ALogTest: ILogTest);
    procedure TestFull();
  end;


implementation

constructor TTestSort.Create(ALog: ILog);
begin
  FLog := ALog;
  FMList := TStringList.Create;
  FUList := TStringList.Create;
end;

destructor TTestSort.Destroy;
begin
  FUList.Free;
  FMList.Free;
  inherited;
end;

function TTestSort.GetCaption: widestring;
begin
  Result := Copy(ClassName, 2, MaxInt);
end;

procedure TTestSort.TestFull;
begin
  FLogTest.StartTest(GetCaption);
  TestSort;
//  FLogTest.Test('1', CompareText('DU', 'du')=0);
  FLogTest.EndTest(GetCaption);
end;

procedure TTestSort.TestProc(ALogTest: ILogTest);
begin
  FLogTest := ALogTest;
  TestFull();
end;

procedure TTestSort.TestUniSortInt();
var
  i: Integer;
  UniSort: IUniSort;
begin
  SetLength(FList, 100);
  for I := 0 to High(FList) do
  begin
    FList[i] := Random(1000*2);
  end;
  UniSort := TUniSort.Create;
  FLogTest.StartTest('Start UniSortInt');
  UniSort.Sort(TSortInt.Create(FList), 0, High(FList));
  FLogTest.EndTest('End UniSortInt');
end;

function TTestSort.GetRandomStr(): AnsiString;
var
  i: Integer;
begin
  SetLength(Result, 5);
  for I := 1 to Length(Result) do
  begin
    Result[i] := AnsiChar(Ord('A') + Random(2));
  end;
end;

procedure TTestSort.TestUniSortStr();
var
  i: Integer;
  UniSort: IUniSort;
  r: Boolean;
begin
  UniSort := TUniSort.Create;
  FLogTest.StartTest('Start UniSortStr');
  UniSort.Sort(TSortStr.Create(FUList), 0, FUList.Count - 1);
  FLogTest.EndTest('End UniSortStr');

  FLogTest.StartTest('Start UniSortStrRes');
  r := true;
  for i := 0 to FUList.Count - 2 do
  begin
    if FUList[i] > FUList[i + 1] then
    begin
      r := false;
      break
    end;
  end;
  FLogTest.Test('Result UniSort', R);
  FLogTest.EndTest('End UniSortStrRes');
end;

procedure TTestSort.TestMSortStr();
var
  i: Integer;
  MergeSort: IMergeSort;
  r: Boolean;
  MergeSortStr: IMergeSortList;
begin
  MergeSort := TMergeSort.Create;
  FLogTest.AddLog('Create MergeSortStr');
  MergeSortStr := TMergeSortStr.Create(FMList);
  FLogTest.StartTest('Start MSortStr');
  MergeSort.Sort(MergeSortStr, FMList.Count);
  FLogTest.EndTest('End MSortStr');
  FLogTest.StartTest('Start MSortStrRes');
  r := true;
  for i := 0 to FMList.Count - 2 do
  begin
    if FMList[i] > FMList[i + 1] then
    begin
      r := false;
      break
    end;
  end;
  FLogTest.Test('Result MSort', R);
  FLogTest.EndTest('End MSortStrRes');
end;

procedure TTestSort.PrintMList();
var
  i: Integer;
begin
    for I := 0 to FMList.Count - 1do
  begin
    FLogTest.AddMsg(FMList[i]);
  end;
end;

procedure TTestSort.PrintUList();
var
  i: Integer;
begin
    for I := 0 to FUList.Count - 1do
  begin
    FLogTest.AddMsg(FUList[i]);
  end;
end;

procedure TTestSort.TestSort();
var
  i: Integer;
begin
  FLogTest.SetShowOK(True);

  FMList.Clear;
  for I := 0 to 4 do
  begin
    FMList.add(string(GetRandomStr));
  end;
  FUList.Assign(FMList);
  PrintMList;
  TestUniSortStr();
  PrintUList;
  TestMSortStr();
  PrintMList;
end;

end.
