unit uFileUtils;

interface
uses
  Winapi.Windows, System.Classes;

procedure SaveRawStringToFile(FileName: String; Text: RawByteString);

procedure SaveTextToFile(FileName, Text: String);

function CreateFile(const AFileName: string): HFILE;

function OpenFile(const AFileName: string): HFILE;

function GetFullFileName(AFileName, AFilePath, AAppPath: string):string;

implementation

function CreateFile(const AFileName: string): HFILE;
begin
  Result := Winapi.Windows.CreateFile(PChar(AFileName),
    GENERIC_WRITE, FILE_SHARE_READ or FILE_SHARE_WRITE or FILE_SHARE_DELETE, nil,
    CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
end;

function OpenFile(const AFileName: string): HFILE;
begin
  Result := Winapi.Windows.CreateFile(PChar(AFileName), GENERIC_READ,
    FILE_SHARE_READ or FILE_SHARE_WRITE or FILE_SHARE_DELETE,
    nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
end;


procedure SaveTextToFile(FileName, Text: String);
begin
  SaveRawStringToFile(FileName, RawByteString(Text));
end;

procedure SaveRawStringToFile(FileName: String; Text: RawByteString);
var
  F: TFileStream;
begin
  F := TFileStream.Create(FileName, fmCreate);
  try
    F.WriteBuffer(Pointer(Text)^, Length(Text));
  finally
    F.Free;
  end
end;

function GetFullFileName(AFileName, AFilePath, AAppPath: string):string;
var
  S: string;
begin
  S := AFileName + #0#0;
  if s[1] = '\' then
  begin
    if S[2] = '\' then
      Result := AFileName // ������ ������� ���� (\\Path)
    else
      Result := AAppPath + Copy(AFileName, 2, MaxInt) // ������������ ����� � �� (\Path)
  end
  else
  begin
    if S[2] = ':' then // ������ ��������� ���� (c:\Path)
      Result := AFileName
    else
      Result := AFilePath + AFileName // ������������� ���� (Path)
  end;
end;

end.
