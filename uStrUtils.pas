unit uStrUtils;

interface
uses
  Ansistrings;

function TryStrToDouble(const S: string; out Value: Double): Boolean;
function StrToDoubleDef(const S: string; DefaultValue: Double): Double;

function GetPureStr(const S: string): string;

function RawByteStrReplace(const S, OldPattern, NewPattern: RawByteString): RawByteString;

function Fmt(Const args: array of variant): string;

implementation

uses
  System.SysUtils, uDateUtils, Variants;

function TryStrToDouble(const S: string; out Value: Double): Boolean;
var
  E: Integer;
begin
  Val(S, Value, E);
  Result := E = 0;
end;

function StrToDoubleDef(const S: string; DefaultValue: Double): Double;
var
  dValue: Double;
begin
  if TryStrToDouble(S, dValue) then
    Result := dValue
  else
    Result := DefaultValue;
end;

function GetPureStr(const S: string): string;
var
  L: integer;
begin
  L := Length(s);
  if (L > 1) and (s[1] = '"') and (s[L] = '"') then
    Result := Copy(s, 2, L - 2)
  else
    Result := s;
end;

function RawBytePos(const Substr, S: RawByteString): Integer;
var
  P: PAnsiChar;
begin
  Result := 0;
  P := Ansistrings.StrPos(PAnsiChar(S), PAnsiChar(SubStr));
//  P := StrPos(PAnsiChar(S), PAnsiChar(SubStr));
  if P <> nil then
    Result := Integer(P) - Integer(PAnsiChar(S)) + 1;
end;

function RawByteStrReplace(const S, OldPattern, NewPattern: RawByteString): RawByteString;
var
  SearchStr, Patt, NewStr: RawByteString;
  Offset: Integer;
begin
  SearchStr := S;
  Patt := OldPattern;

  NewStr := S;
  Result := '';
  while SearchStr <> '' do
  begin
    Offset := RawBytePos(Patt, SearchStr);
    if Offset = 0 then
    begin
      Result := Result + NewStr;
      Break;
    end;
    Result := Result + Copy(NewStr, 1, Offset - 1) + NewPattern;
    NewStr := Copy(NewStr, Offset + Length(OldPattern), MaxInt);
//    if not (rfReplaceAll in Flags) then
//    begin
//      Result := Result + NewStr;
//      Break;
//    end;
    SearchStr := Copy(SearchStr, Offset + Length(Patt), MaxInt);
  end;
end;

procedure StringInclude(sInclude: string; var S: string; var Len: integer);
var
  LInc: integer;
begin
  LInc := Length(sInclude);
  if LInc > 0 then
  begin
    if Length(S) < Len + LInc then
     SetLength(s, (Len + LInc) * 2);
    move(sInclude[1], s[Len + 1], LInc * sizeof(char));
    inc(Len, LInc);
  end;
end;

function Fmt(Const args: array of variant): string;
var
  i, LRes: integer;
  s: string;
  a: variant;
begin
  LRes := 0;
  for i := low(args) to high(args) do
  begin
    a := args[i];
    if TVarData(a).VType = varDate then
      s := DateTimeToISO(TVarData(a).VDate)
    else
      s := vartostr(a);
    StringInclude(s, result, LRes);
  end;
  SetLength(Result, LRes)
end;

end.
