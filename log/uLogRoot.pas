﻿unit uLogRoot;

interface

uses
  windows, sysutils, uLogintf, uDateUtils, strutils;

type
  TLogData = class(TInterfacedObject, ILogData)
    FSysTime: TSysDateTime;
    procedure NewSysDate();
    function GetSysDateTime: PSysDateTime;
    function GetSysDate(): TSysDate;
    function GetSysTime(): TSysTime;
    function GetHead(): string;
  end;

  TLogRoot = class(TInterfacedObject, ILogRoot)
  //Пишет в указанный файл
  private
    FLogData: ILogData;
    FWriter: ILogWriter; //Писатель
    FLastTick: dword; //Последний TickCount, 0 - если не было записи
    function TickStr(): string;
  protected
    { ILogData }
//    function GetSysDate(): TSysDate;
//    function GetSysTime(): TSysTime;
//    function GetHead(): string;
    { /ILogData }
    { ILogRoot }
    procedure Log(const AMode: TLogMode; const ObjectName, AMsg: string); overload;
//    function GetLogFile(AFileName: string): ILogRoot; //Лог в другой файл
    { /ILogRoot }
  public
    constructor Create(AWriter: ILogWriter; ALogData: ILogData);
    destructor Destroy; override;
  end;

implementation

function GetLogModeStr(const AMode: TLogMode): string;
const
 LogModes: array [TLogMode] of string = ('', 'D; ', 'T; ', 'I; ', 'W; ', 'E; ', 'C; ', 'F; ');
// TLogMode = (lmNone, lmDebug, lmTrace, lmInfo, lmWarning, lmError, lmCritical, lmFatal);
begin
  Result := LogModes[AMode];
end;

function TLogData.GetHead: string;
begin
  Result := 'Date       Time         [   Tick] Mode; Object. Message';
         //('2014-23-43 33:22:43.123 [123 456] Mode; Object. Message');
end;

procedure TLogData.NewSysDate();
begin
  Windows.GetLocalTime(FSysTime.SystemTime);
end;

function TLogData.GetSysDate(): TSysDate;
begin
  result := FSysTime.Date;
end;

function TLogData.GetSysDateTime: PSysDateTime;
begin
  Result := @FSysTime;
end;

function TLogData.GetSysTime: TSysTime;
begin
  result := FSysTime.Time;
end;

constructor TLogRoot.Create(AWriter: ILogWriter; ALogData: ILogData);
begin
  inherited Create;
  FWriter := AWriter;
  FLogData :=  ALogData;
//  FWriter.SetData(Self);
end;

destructor TLogRoot.Destroy;
begin

  inherited;
end;

function TLogRoot.TickStr(): string;
var
  dwTick: DWORD;
begin
  dwTick := GetTickCount;
  if FLastTick > 0 then
  begin
    Result := IntToSStr(dwTick - FLastTick);
    if Length(Result) < 7 then
      Result := RightStr('      ' + Result, 7)
  end
  else
    Result := '-------';
  FLastTick := dwTick;
//  if Length(Result) < 7 then
//    Result := RightStr('      ' + Result, 7)
end;

procedure TLogRoot.Log(const AMode: TLogMode; const ObjectName, AMsg: string);
var
  ObjectSeparator: string;
begin
  FLogData.NewSysDate();
  if ObjectName <> '' then
    ObjectSeparator := '. ';
//  s := format('%s  [%7s] %s%s %s'#13#10,[SysDateTimeToIso(st), TickStr(), GetLogModeStr(AMode), ObjectName, AMsg]);
  FWriter.WriteLn(SysDateTimeToIsoZ(FLogData.GetSysDateTime) + ' [' + TickStr() + '] ' + GetLogModeStr(AMode) + ObjectName + ObjectSeparator + AMsg);
end;

end.
