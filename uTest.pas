unit uTest;

interface
type
  ILogTest = interface
    procedure AddLog(s: string);
    procedure AddMsg(Descr: string);
    procedure Test(Descr: string; Succesful: Boolean);
    procedure StartTest(Descr: string);
    procedure EndTest(Descr: string);
    procedure SetShowOK(aShowOK: Boolean);
  end;

  ITest = interface
  ['{479CA5C7-918C-4533-93F1-13D9DC76AE2E}']
    function GetCaption: WideString;
    procedure TestProc(ALogTest: ILogTest);
  end;

  ITestList = interface
    function GetCount:Integer;
    function GetItem(index: Integer):ITest;
  end;

implementation

end.
