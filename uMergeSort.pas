unit uMergeSort;

interface
type
  IMergeSortList = interface
    procedure Swap();
    function Compare(Index1, Index2: Integer): Integer;
    procedure SaveItem(Dest, Src: Integer);
    procedure SaveItems(Dest, Src, Count: Integer);
    //procedure ExchangeItems(Index1, Index2: Integer);
  end;

  IMergeSort = interface
    procedure Sort(List: IMergeSortList; ACount: Integer);
  end;

  TMergeSort = class(TInterfacedObject, IMergeSort)
  private
    FList: IMergeSortList;
    FCount: integer;
    procedure Merge(L1, L2, L3: Integer);
  public
    procedure Sort(List: IMergeSortList; ACount: Integer);
    procedure MergeSort(AStep: Integer);
  end;

implementation

procedure TMergeSort.Sort(List: IMergeSortList; ACount: Integer);
var
  AStep: integer;
begin
  FList := List;
  FCount := ACount;
  AStep := 1;
  while AStep < ACount do
  Begin
    MergeSort(AStep);
    AStep := AStep shl 1;
    FList.Swap;
  End;
end;

procedure TMergeSort.MergeSort(AStep: Integer);
var
  L1, L2, L3: Integer;
begin
  L1 := 0;
  while L1 < FCount do
  begin
    L2 := L1 + AStep;
    L3 := L2 + AStep;
    if L3 > FCount then
    begin
      if L2 >= FCount then
      begin
        FList.SaveItems(L1, L1, FCount - L1);
        Break;
      end;
      L3 := FCount;
    end;
    Merge(L1, L2, L3);
    L1 := L3
  end;
end;

{
 1  2  3  4
 5  6  7  8
 9 10 11

 1 , 2 ! 3 , 4 ! 5 , 6
 1,2,3 ; 4,5,6,7

 7,8,9,10;11,12,13,14
 7,8,9,10;11,12
 7,8,9,10;11,12,13,14

 12,13,14,15;7,8
 7
}

procedure TMergeSort.Merge(L1, L2, L3: Integer);
var
  A, B, I: Integer;
begin
  A := L1;
  B := L2;
  I := L1;
  while True do
    if FList.Compare(A, B) < 0 then
    begin
      FList.SaveItem(I, A);
      Inc(I);
      Inc(A);
      if A >= L2 then
      begin
        FList.SaveItems(I, B, L3 - B);
        Break;
      end;
    end
    else
    begin
      FList.SaveItem(I, B);
      Inc(I);
      Inc(B);
      if B >= L3 then
      begin
        FList.SaveItems(I, A, L2 - A);
        Break;
      end;
    end;
end;

end.
