﻿unit uLogObj;

interface
uses
  uLogintf, SysUtils, uStrUtils;

type
  TLogObj = class(TInterfacedObject, ILog)
  //Пишет в указанный файл
  private
    FLogRoot: ILogRoot;
    FObjName: string;
  protected
    { ILog }
    procedure Log(const AMode: TLogMode; const AMsg: string); overload;
    procedure Log(const AMode: TLogMode; const AFormat: string; const Args: array of const); overload;
    procedure Log(const AMode: TLogMode; const Args: array of variant); overload;
    function GetLog(AObjName: string): ILog; // лог следующего уровня
    function GetRoot(AObjName: string): ILog; // лог другого обекта
    function GetSib(AObjName: string): ILog; // Заменить имя последнего объекта
    { /ILog }
  public
    constructor Create(ALogRoot: ILogRoot; AObjName: string);
    destructor Destroy; override;
  end;

implementation

{ TLogObj }

constructor TLogObj.Create(ALogRoot: ILogRoot; AObjName: string);
begin
  FLogRoot := ALogRoot;
  FObjName := AObjName;
end;

destructor TLogObj.Destroy;
begin

  inherited;
end;

function TLogObj.GetSib(AObjName: string): ILog;
var
  i: integer;
  ObjPath: string;
begin
  i := LastDelimiter('.', FObjName);
  if i > 0 then
    ObjPath := Copy(FObjName, 1, i - 1) + '.'
  else
    ObjPath := '';
  Result := TLogObj.Create(FLogRoot, ObjPath + AObjName);
end;

function TLogObj.GetRoot(AObjName: string): ILog;
begin
  Result := TLogObj.Create(FLogRoot, AObjName);
end;

function TLogObj.GetLog(AObjName: string): ILog;
begin
  if FObjName <> '' then
    AObjName := FObjName + '.' + AObjName;
  Result := TLogObj.Create(FLogRoot, AObjName);
end;

procedure TLogObj.Log(const AMode: TLogMode; const AMsg: string);
begin
  FLogRoot.Log(AMode, FObjName, AMsg);
end;

procedure TLogObj.Log(const AMode: TLogMode; const Args: array of variant);
begin
  Log(AMode, uStrUtils.Fmt(Args));
end;

procedure TLogObj.Log(const AMode: TLogMode; const AFormat: string;
  const Args: array of const);
begin
  Log(AMode, SysUtils.Format(AFormat, Args));
end;

end.
