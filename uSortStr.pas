unit uSortStr;

interface

uses
  System.SysUtils, System.Types, System.Classes,
  uSortList;

type
  TSortStr = class(TInterfacedObject, ISortList)
  private
    FList: TStrings;
  public
    constructor Create(const AList: TStrings);
    function Compare(Index1, Index2: Integer): Integer;
    procedure ExchangeItems(Index1, Index2: Integer);
  end;

implementation

function CompareIntegerAsc(Item1, Item2: Integer): Integer;
begin
  if Item1>Item2 then
    Result := 1
  else
  if Item1<Item2 then
    Result := -1
  else
    Result := 0;
end;

{ TSortGroups }

function TSortStr.Compare(Index1, Index2: Integer): Integer;
begin
  Result := CompareStr(FList[Index1], FList[index2]);
end;

constructor TSortStr.Create(const AList: TStrings);
begin
  FList := AList;
end;

procedure TSortStr.ExchangeItems(Index1, Index2: Integer);
var
  Item1: string;
begin
  Item1 := FList[Index1];
  FList[Index1] := FList[Index2];
  FList[Index2] := Item1;
end;

end.
