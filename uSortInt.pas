unit uSortInt;

interface

uses
  System.Types, uSortList;

type
  TSortInt = class(TInterfacedObject, ISortList)
  private
    FList: TIntegerDynArray;
  public
    constructor Create(const AList: TIntegerDynArray);
    function Compare(Index1, Index2: Integer): Integer;
    procedure ExchangeItems(Index1, Index2: Integer);
  end;

implementation

function CompareIntegerAsc(Item1, Item2: Integer): Integer;
begin
  if Item1>Item2 then
    Result := 1
  else
  if Item1<Item2 then
    Result := -1
  else
    Result := 0;
end;

{ TSortGroups }

function TSortInt.Compare(Index1, Index2: Integer): Integer;
begin
  Result := CompareIntegerAsc(FList[Index1],FList[index2]);
end;

constructor TSortInt.Create(const AList: TIntegerDynArray);
begin
  FList := AList;
end;

procedure TSortInt.ExchangeItems(Index1, Index2: Integer);
var
  Item1: Integer;
begin
  Item1 := FList[Index1];
  FList[Index1] := FList[Index2];
  FList[Index2] := Item1;
end;

end.
