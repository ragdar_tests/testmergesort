unit uMainForm;

interface

uses
  Windows, SysUtils, Classes, System.Contnrs,
  vcl.Controls, vcl.Forms, vcl.StdCtrls, vcl.ComCtrls,
  uTest, uTestUtils;

type
  TfmMain = class(TForm)
  private
    FLastLeft, FLastTop: integer;
    Fmem: TMemo;
    FToolBar: TToolBar;
    FTestList: TInterfaceList;
    FObjectList: TObjectList;
    FLogTest: ILogTest;
//    FLog: ILog;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure CreateComponents();
    function CreateToolButton(aCaption: TCaption; ClickProc: TNotifyEvent): TToolButton;
    procedure InsertToolControl(w: TControl);

    procedure ClearMemo(Sender: TObject);
    procedure ClickForTest(Sender: TObject);

    procedure AddTestObject(ATest: ITest);
    procedure StartAllTest();
    procedure AddChildObject(AObject: TObject);
  end;

implementation

uses
  uTestMan;

{ TfmMain }
procedure TfmMain.AddChildObject(AObject: TObject);
begin
  FObjectList.Add(AObject);
end;

procedure TfmMain.AddTestObject(ATest: ITest);
var
  btnTest: TToolButton;
begin
  FTestList.Add(ATest);
  btnTest := CreateToolButton(ATest.GetCaption, ClickForTest);
  btnTest.Tag := Integer(Pointer(ATest));
end;

procedure TfmMain.ClearMemo(Sender: TObject);
begin
  Fmem.Clear;
end;

constructor TfmMain.Create(AOwner: TComponent);
begin
  inherited CreateNew(AOwner);
  FTestList := TInterfaceList.Create;
  FObjectList := TObjectList.Create;
  Position:=poDefault;
  Visible:=True;
  CreateComponents;

  FLogTest := TTestManager.Create(Fmem.Lines);
  SetLogTest(FLogTest);
  Caption := Application.Title;
end;

destructor TfmMain.Destroy;
begin
  FObjectList.Free;
  FTestList.Free;
  inherited;
end;

procedure TfmMain.CreateComponents;
begin
  FToolBar := TToolBar.Create(Self);
  FToolBar.EdgeBorders := [];
  FToolBar.ShowCaptions := True;
  FToolBar.AutoSize := true;
  FToolBar.Flat := True;
  FToolBar.Height := 21;
  FToolBar.Parent := Self;

  Fmem := TMemo.create(Self);
  Fmem.ScrollBars := ssBoth;
  Fmem.Top := FToolBar.BoundsRect.Bottom;
  Fmem.Font.Name := 'Courier New';
  Fmem.Font.Height := 16;
  Fmem.Align := alClient;
  Fmem.Parent := Self;

//  CreateToolButton('Close', FormClose);
  CreateToolButton('Clear', ClearMemo);
end;

const
  ButtonBorder = 13;
function TfmMain.CreateToolButton(aCaption: TCaption; ClickProc: TNotifyEvent): TToolButton;
begin
  Result := TToolButton.Create(Self);
  Result.OnClick := ClickProc;
  Result.Caption := aCaption;
  Result.Width := Canvas.TextWidth(aCaption) + ButtonBorder;
  Result.AutoSize := true;
  InsertToolControl(Result);
  //  Result.Parent:=FToolBar;
end;

procedure TfmMain.InsertToolControl(w: TControl);
var
  iHeight: integer;
begin
  iHeight := W.Height;
  if FLastLeft + w.Width > Screen.Width then
  begin
    FLastLeft := 0;
    FLastTop := FLastTop + iHeight;
  end;
  FToolBar.Height := FLastTop + iHeight;
  W.Top := FLastTop;
  W.Left := FLastLeft;
  FLastLeft := W.BoundsRect.Right;
  W.Parent := FToolBar;
end;

procedure TfmMain.ClickForTest(Sender: TObject);
var
  Test: ITest;
begin
//  FLog.GetLog('').Log(lmTrace, 'FLog.GetLog('''').Log');
//  FLog.GetLog('Object').Log(lmTrace, 'FLog.GetLog(''Object'').Log');

  Integer(Test) := (Sender as TToolButton).Tag;
  Test._AddRef;
  Test.TestProc(FLogTest);
end;

procedure TfmMain.StartAllTest;
var
  i: integer;
begin
  For i := 0 to FTestList.Count-1 do
    (FTestList.Items[i] as ITest).TestProc(FLogTest);
end;

end.
