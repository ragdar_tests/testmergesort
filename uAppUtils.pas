unit uAppUtils;

interface

function GetAppPath(): string;

function GetAppName: string;

function GetAppTitle(): string;

function GetCfgPath(): string;
function GetCfgSysPath(): string;

function GetParserPath(): string;
function GetParserSysPath(): string;

function GetInfoPath(): string;
function GetLogPath(): string;

implementation

uses
  System.SysUtils;

function LastChar(const c: char; const S: string): Integer;
begin
  Result := Length(S);
  while Result > 0 do
  begin
    if S[Result] = c then
      Exit;
    Dec(Result);
  end;
end;

function GetAppName: string;
begin
  result := ExtractFileName(ParamStr(0));
end;

function GetAppTitle(): string;
var
  I: Integer;
  FileName: string;
begin
  FileName := GetAppName;
  I := LastChar('.', FileName);
  if I > 0 then
    Result := Copy(FileName, 1, i - 1)
  else
    Result := FileName;
end;

function GetAppPath(): string;
begin
  Result := ExtractFilePath(ParamStr(0));
end;

function GetCfgPath(): string;
begin
  Result := GetAppPath()+'cfg\';
end;

function GetCfgSysPath(): string;
begin
  Result := GetCfgPath+'sys\';
end;

function GetParserPath(): string;
begin
  Result := GetCfgPath+'parsers\';
end;

function GetParserSysPath(): string;
begin
  Result := GetParserPath+'sys\';
end;

function GetInfoPath(): string;
begin
  Result := GetAppPath()+'info\';
end;

function GetLogPath(): string;
begin
  Result := GetAppPath()+'log\';
end;

end.
