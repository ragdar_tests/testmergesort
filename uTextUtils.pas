unit uTextUtils;

interface
uses
  Windows, System.SysUtils, uLogintf;

function LoadText(FileName: string; var Data: AnsiString): Boolean;

function LoadTextFile(FileName: string; var Data: AnsiString): Integer;

function SaveText(FileName: string; Data: AnsiString): Boolean;

function FileOpenExisting(const AFileName: string): HFILE;

function FileCreateAlways(const AFileName: string): HFILE;

function LogLoadText(Log: ILog; FileName: string; var AData: string): Boolean;

implementation

function FileOpenExisting(const AFileName: string): HFILE;
begin
  Result := Windows.CreateFile(PChar(AFileName), GENERIC_READ,
    FILE_SHARE_READ or FILE_SHARE_WRITE or FILE_SHARE_DELETE, nil,
    OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
end;

function FileCreateAlways(const AFileName: string): HFILE;
begin
  Result := Windows.CreateFile(PChar(AFileName), GENERIC_WRITE,
    0, nil,
    CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
end;

function LoadTextFile(FileName: string; var Data: AnsiString): Integer;
var
  h: THandle;
  Size, NumberOfBytesRead: Cardinal;
begin
  Result := 0;
  h := FileOpenExisting(FileName);
  if h <> INVALID_HANDLE_VALUE then
    try
      Size := SetFilePointer(H, 0, nil, FILE_END);
      SetLength(Data, Size);
      SetFilePointer(H, 0, nil, FILE_BEGIN);
      if Size > 0 then
        if not Windows.ReadFile(H, Pointer(Data)^, Size, NumberOfBytesRead, nil) then
          Result := GetLastError;
    finally
      CloseHandle(h)
    end
  else
    Result := GetLastError;
end;

function LoadText(FileName: string; var Data: AnsiString): Boolean;
var
  h: THandle;
  Size, NumberOfBytesRead: Cardinal;
begin
//  H := FileOpen(FileName, fmOpenRead)
  h := FileOpenExisting(FileName);
  Result := h <> INVALID_HANDLE_VALUE;
  if Result then
  try
    Size := SetFilePointer(H, 0, nil, FILE_END);
    SetLength(Data, Size);
    SetFilePointer(H, 0, nil, FILE_BEGIN);
    if Size > 0 then
      Result := Windows.ReadFile(H, Pointer(Data)^, Size, NumberOfBytesRead, nil);
  finally
    CloseHandle(h)
  end;
end;

function LogLoadText(Log: ILog; FileName: string; var AData: string): Boolean;
var
  Data: AnsiString;
begin
  Result := FileExists(FileName);
  if Result then
  begin
    Log.Log(lmInfo, 'Loading '+ FileName);
    Result := LoadText(FileName, Data);
    AData := string(Data)
  end
  else
    Log.Log(lmError, 'file "'+ FileName + '" not found');
end;

function SaveText(FileName: string; Data: AnsiString): Boolean;
var
  h: THandle;
  NumberOfBytesRead: Cardinal;
begin
  h := FileCreateAlways(FileName);
  Result := h <> INVALID_HANDLE_VALUE;
  if Result then
  try
    Result := Windows.WriteFile(H, Pointer(Data)^, Length(Data), NumberOfBytesRead, nil);
  finally
    CloseHandle(h)
  end;
end;

end.
